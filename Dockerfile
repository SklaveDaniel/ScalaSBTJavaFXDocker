FROM  openjdk:8

ENV SCALA_VERSION 2.12.8
ENV SBT_VERSION 1.2.8

# Scala expects this file
RUN touch /usr/lib/jvm/java-8-openjdk-amd64/release

# Install Scala
## Piping curl directly in tar
RUN \
  curl -fsL http://downloads.typesafe.com/scala/$SCALA_VERSION/scala-$SCALA_VERSION.tgz | tar xfz - -C /root/ && \
  echo >> /root/.bashrc && \
  echo 'export PATH=~/scala-$SCALA_VERSION/bin:$PATH' >> /root/.bashrc

# Install sbt
RUN \
  curl -L -o sbt-$SBT_VERSION.deb http://dl.bintray.com/sbt/debian/sbt-$SBT_VERSION.deb && \
  dpkg -i sbt-$SBT_VERSION.deb && \
  rm sbt-$SBT_VERSION.deb && \
  apt-get update && \
  apt-get install sbt && \
  sbt sbtVersion

# Install JavaFX
RUN \
  apt-get install -y --no-install-recommends openjfx

# Install wixl
RUN \
  apt-get install -y --no-install-recommends wixl

# Fetch Windows JREs
RUN \
  cd /root && \
  wget https://gitlab.com/SklaveDaniel/ScalaSBTJavaFXDocker/wikis/jre-8u144-windows-i586.tar.gz && \
  wget https://gitlab.com/SklaveDaniel/ScalaSBTJavaFXDocker/wikis/jre-8u144-windows-x64.tar.gz && \
  wget https://gitlab.com/SklaveDaniel/ScalaSBTJavaFXDocker/wikis/jre-11.0.2-windows-x64-custom.tar.gz && \
  mkdir jre32 && \
  tar -xzf jre-8u144-windows-i586.tar.gz -C jre32 --strip-components=1 && \
  mkdir jre64 && \
  tar -xzf jre-8u144-windows-x64.tar.gz -C jre64 --strip-components=1 && \
  mkdir jre-11-win-64 && \
  tar -xzf jre-11.0.2-windows-x64-custom.tar.gz -C jre-11-win-64 --strip-components=1

# Define working directory
WORKDIR /root

